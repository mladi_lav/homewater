$(document).ready(function () {

    //TOOGLE HIDDEN
    $(document).on('click', '[data-toggle-hidden]', function () {
        var btn = $(this),
            block = $(btn.data('toggle-hidden')),
            isActive = btn.hasClass('active');

        btn[isActive ? 'removeClass' : 'addClass']('active');
        block[isActive ? 'removeClass' : 'addClass']('open');
    });


    //OPEN

    $(document).on('click', '[data-open]', function () {
        var btn = $(this),
            block = $(btn.data('open')),
            isActive = block.hasClass('open');

        block[isActive ? 'removeClass' : 'addClass']('open');
    });


    //PRODUCTS SLIDER

    var products = new Swiper('.swiper-products', {
        slidesPerView: 4,
        spaceBetween: 30,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        breakpoints: {
            320: {
                slidesPerView: 1
            },
            768: {
                slidesPerView: 2
            },
            1080: {
                slidesPerView: 3,
            }
        }
    });

    //TESTIMONIALS SLIDER

    var testimonials = new Swiper('.swiper-testimonials', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });


    //INPUT CHANGE
    $(document).on('change', '.group-label-up input, .group-label-up textarea', function () {
        setLabelInput($(this));

    });


    //PRODUCT SLIDER
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        direction: 'vertical',
        slideToClickedSlide: true
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;


    //CHANGE QUANTITY
    $(document).on('click', '.quantity .btn', function (e) {

        var btn = $(this),
            input = btn.siblings('input'),
            isPlus =  btn.hasClass('plus'),
            value = parseInt(input.val());

        value = isPlus ? value + 1 : value - 1;

        if(value <= 0) value = 1;
        input.val(value);
    });

    $( "select" ).selectmenu({
        change: function( event, data ) {
            setLabelInput($(this));
        }
    });


    prepareInput();

    $(document).on('hidden.bs.modal', function (event) {
        if ($('.modal:visible').length) {
            $('body').addClass('modal-open');
        }
    });


    //$('#offer-thanks').modal('show');



    $(document).on('click', '[data-spin]', function () {

        $(this).prop('disabled', true);
        spinCircle();

    });

    var date = getDate() + ' ' + $('#clock').data('time');
    $('#clock').countdown(date, function(event) {
        var $this = $(this).html(event.strftime(''
            + '<div class="item"><div>%H</div> <span>hours</span></div>'
            + '<div class="item"><div>%M</div> <span>min</span></div>'
            + '<div class="item"><div>%S</div> <span>sec</span></div>'));
    });

});


function getDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    }

    if(mm<10) {
        mm='0'+mm
    }

    today = yyyy + '/' + mm + '/' + dd;
    return today;
}

function spinCircle() {
    var number = getRandomInt(1, 7),
        spin = 360 * 10 + (number * 60);

    $('.discounts').css({transform: 'rotate(' + spin + 'deg) scale(1)' });

    setTimeout(function () {
        $('.discounts .item').eq(5 - (number - 1)).addClass('active');
        var active =  $('.discounts .item.active'),
            id = active.data(id),
            text =  active.text();
        showWinModal(text, id);
    }, 1000);

}


function showWinModal(text, id) {

    // id - записать в сессию или куки чтобы не смогли менять в форме

    $('#circle-win .win-off-text').text(text);

    setTimeout(function () {
        $('#circle-spin').modal('hide');
        $('#circle-win').modal('show');
    }, 1000);

}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function prepareInput() {
    $('.group-label-up input, .group-label-up select, .group-label-up textarea').each(function (indx, e) {
        setLabelInput($(e));
    });
}

function setLabelInput(input) {

    var value = input.val(),
        notEmpty;

    if(input.val() == null) {
        notEmpty = false;
    } else {
        notEmpty = value.length > 0;
    }

    input[notEmpty ? 'addClass' : 'removeClass']('active')

}
